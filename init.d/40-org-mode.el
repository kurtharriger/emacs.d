;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.


(add-to-list 'load-path "lib/org-mode/lisp")
(add-to-list 'load-path "lib/org-mode/contrib/lisp")
(require 'org-install)
