

(defun  mark-to-end-of-line ()
  (interactive)
  (set-mark-command nil)
  (move-end-of-line nil)
)

(defun  mark-to-beginning-of-line ()
  (interactive)
  (set-mark-command nil)
  (move-beginning-of-line nil)
)


(global-set-key (kbd  "<s-left>") 'move-beginning-of-line) 
(global-set-key (kbd  "<s-right>") 'move-end-of-line)
(global-set-key (kbd  "<s-S-left>") 'move-beginning-of-line)
(global-set-key (kbd  "<s-S-right>") 'mark-to-end-of-line)



(browse-kill-ring-default-keybindings)

