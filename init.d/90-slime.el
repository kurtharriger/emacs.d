
;; (setq slime-protocol-version 'ignore) 
;; (setq clojure-swank-command
;;   (if (or (locate-file "lein" exec-path) (locate-file "lein.bat" exec-path))
;;     "lein ritz-in %s"
;;    "echo \"lein ritz-in %s\" | $SHELL -l"))

(defun slime-redirect-stdout ()
  (interactive)
  (slime-interactive-eval
   "(System/setOut (java.io.PrintStream. (org.apache.commons.io.output.WriterOutputStream. *out* \"UTF-8\" 8192 true)))"))

(defun slime-bootstrap-clojure ()
  (interactive)
  (slime-interactive-eval
   "(require '[clojure.java.io :as io])
    (require '[clojure.walk :as walk])
    (require '[clojure.repl :as repl])
    (require '[clojure.reflect :as reflect])
    (require '[clojure.pprint :as pprint])
    (require '[clojure.string :as string])"))

(defun clojure-mode-slime-font-lock ()
  (require 'clojure-mode)
  (paredit-mode)
  (let (font-lock-mode)
    (clojure-mode-font-lock-setup)))

(add-hook 'slime-repl-mode-hook 'clojure-mode-slime-font-lock)
(add-hook 'slime-connected-hook 'slime-bootstrap-clojure)
;;(add-hook 'slime-connected-hook 'slime-redirect-stdout)

