(defvar autosave-dir
 (concat dotfiles-dir "autosaves/"))
(make-directory autosave-dir t)
(defun auto-save-file-name-p (filename)
  (string-match "^#.*#$" (file-name-nondirectory filename)))
(defun make-auto-save-file-name ()
  (concat autosave-dir
   (if buffer-file-name
      (concat "#" (file-name-nondirectory buffer-file-name) "#")
    (expand-file-name
     (concat "#%" (buffer-name) "#")))))


;; Keep emacs in sync with file system and vs versa
(setq auto-save-list-file-prefix nil)
(setq auto-save-interval 0)
(setq auto-save-timeout 1)
(setq create-lockfiles nil)
(setq global-auto-revert-mode t)


;; (setq auto-save-visited-file-name t) doesn't seem to work, try hook instead
(setq auto-save-visited-file-name t)
(defun save-buffer-if-visiting-file (&optional args)
  "Save the current buffer only if it is visiting a file"
  (interactive)
  (if (buffer-file-name)
      (save-buffer args)))

(defun make-auto-save-file-name ()
  nil)

(defun make-auto-save-file-name-p ()
  nil)


(add-hook 'auto-save-hook 'save-buffer-if-visiting-file)
(add-hook 'first-change-hook 'save-buffer-if-visiting-file)
