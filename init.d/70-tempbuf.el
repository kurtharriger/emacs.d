(require 'tempbuf)

(add-hook 'dired-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'custom-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'w3-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'Man-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'view-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'fundamental-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'buffer-menu-mode-hook 'turn-on-tempbuf-mode)
(add-hook 'temp-buffer-setup-hook 'turn-on-tempbuf-mode)
(add-hook 'lisp-mode 'turn-on-tempbuf-mode)



;;(add-hook 'find-file-hooks 'turn-on-tempbuf-mode)
;;(add-hook 'after-change-major-mode-hook 'turn-on-tempbuf-mode) 
;;(add-hook 'find-file-hooks 'turn-off-tempbuf-mode)






