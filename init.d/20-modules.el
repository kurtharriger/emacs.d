
(setq modules-dir (concat dotfiles-dir "modules"))

(when (file-directory-p modules-dir)
  (let ((modules-dirs (directory-files modules-dir nil "^[^.]")))
    (dolist (dir modules-dirs) 
      (add-to-list 'load-path (expand-file-name dir modules-dir)))))
       

